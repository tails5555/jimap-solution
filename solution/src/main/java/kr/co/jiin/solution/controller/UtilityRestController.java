package kr.co.jiin.solution.controller;

import kr.co.jiin.solution.util.ConverterUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

@RestController
@RequestMapping("api")
public class UtilityRestController {
    @PostMapping("convert/xml")
    public String convertJSONToXML(@RequestBody String json) throws UnsupportedEncodingException {
        return ConverterUtil.convertJSONToXML(URLDecoder.decode(json, "UTF-8"));
    }
}
