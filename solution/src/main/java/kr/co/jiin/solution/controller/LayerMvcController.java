package kr.co.jiin.solution.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("layer")
public class LayerMvcController {
    @RequestMapping("list")
    public String layerListView(Model model){
        return "page/layer/list";
    }

    @RequestMapping("preview")
    public String layerPreview(Model model, @RequestParam String id){
        return "page/layer/view";
    }

    @RequestMapping("create")
    public String layerCreateView(Model model){
        return "page/layer/form";
    }

    @RequestMapping("update")
    public String layerUpdateView(Model model, @RequestParam String id){
        return "page/layer/edit";
    }
}
