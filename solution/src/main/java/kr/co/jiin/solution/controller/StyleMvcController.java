package kr.co.jiin.solution.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("style")
public class StyleMvcController {
    @RequestMapping("list")
    public String styleListView(Model model){
        return "page/style/list";
    }

    @RequestMapping("preview")
    public String stylePreview(Model model, @RequestParam String id){
        return "page/style/view";
    }

    @RequestMapping("create")
    public String styleCreateView(Model model){
        return "page/style/form";
    }

    @RequestMapping("update")
    public String styleUpdateView(Model model, @RequestParam String id){
        return "page/style/edit";
    }

    @RequestMapping("converter")
    public String styleConverter(Model model) {
        return "page/style/converter";
    }
}
