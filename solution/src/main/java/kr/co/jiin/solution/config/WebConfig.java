package kr.co.jiin.solution.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

@Configuration
@PropertySource("classpath:config/template.properties")
public class WebConfig {
    @Value("${resources.static-locations}")
    private String resourcesStaticLocations;

    @Value("${resources.cache-period}")
    private Integer resourcesCachePeriod;

    @Value("${thymeleaf.prefix}")
    private String thymeleafPrefix;

    @Value("${thymeleaf.cache}")
    private boolean thymeleafCache;

    /**
     * ITemplateResolver Bean 객체 생성 : Thymeleaf 에서 UTF-8 환경을 이루기 위한 메소드.
     * @ param
     * @ return ITemplateResolver 객체
     */
    @Bean
    public ITemplateResolver templateResolver() {
        SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
        resolver.setPrefix(thymeleafPrefix);
        resolver.setSuffix(".html");
        resolver.setCacheable(thymeleafCache);
        resolver.setCharacterEncoding("UTF-8");
        return resolver;
    }

    /**
     * ServletRegistrationBean Bean 객체 생성 : Web 서비스를 /web URI 에서 사용하기 위한 메소드
     * @ param
     * @ return ServletRegistrationBean 객체
     */
    @Bean
    public ServletRegistrationBean<DispatcherServlet> servletWeb() {
        AnnotationConfigWebApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();
        applicationContext.register(WebConfig.class);
        DispatcherServlet dispatcherServlet = new DispatcherServlet();
        dispatcherServlet.setApplicationContext(applicationContext);
        dispatcherServlet.setThrowExceptionIfNoHandlerFound(true);
        final ServletRegistrationBean<DispatcherServlet> bean = new ServletRegistrationBean<>(dispatcherServlet, "/web/*");
        bean.setName("web");
        bean.setLoadOnStartup(1);
        return bean;
    }
}
