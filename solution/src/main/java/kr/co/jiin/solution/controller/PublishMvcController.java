package kr.co.jiin.solution.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("publish")
public class PublishMvcController {
    @RequestMapping("{relative}")
    public String publishPreview(Model model, @PathVariable String relative){
        return String.format("publish/%s", relative);
    }

    @RequestMapping("home")
    public String publishHomeView(Model model) {
        return "publish/s00-dash-board";
    }
}
