package kr.co.jiin.solution.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("system")
public class SystemMvcController {
    @RequestMapping("version")
    public String systemVersion(Model model){
        return "page/system/version";
    }
}
