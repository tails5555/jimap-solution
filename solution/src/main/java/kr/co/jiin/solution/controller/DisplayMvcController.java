package kr.co.jiin.solution.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("display")
public class DisplayMvcController {
    @RequestMapping("wms")
    public String wmsDisplay(){
        return "page/display/wms";
    }

    // Mapbox 지도 도시
    @RequestMapping("mapbox")
    public String mapboxDisplay(){
        return "page/display/mapbox";
    }

    // 3차원 지도 도시
    @RequestMapping("sphere")
    public String sphereDisplay(){
        return "page/display/sphere";
    }
}
