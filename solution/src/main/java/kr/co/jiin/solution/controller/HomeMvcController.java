package kr.co.jiin.solution.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("home")
public class HomeMvcController {
    @RequestMapping("guest")
    public String homeViewForGuest(Model model){
        return "page/home/guest";
    }

    @RequestMapping("user")
    public String homeViewForUser(Model model){
        return "page/home/user";
    }
}
