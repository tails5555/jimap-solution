package kr.co.jiin.solution.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("vector")
public class VectorMapMvcController {
    @RequestMapping("list")
    public String vectorDataListView(Model model){
        return "page/vector/list";
    }

    @RequestMapping("upload")
    public String vectorDataUploadView(Model model){
        return "page/vector/upload";
    }
}
