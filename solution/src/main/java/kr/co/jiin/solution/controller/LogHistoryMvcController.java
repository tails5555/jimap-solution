package kr.co.jiin.solution.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("log")
public class LogHistoryMvcController {
    @RequestMapping("display")
    public String logDisplay(Model model){
        return "page/log/display";
    }

    @RequestMapping("config")
    public String logConfig(Model model){
        return "page/log/config";
    }
}
