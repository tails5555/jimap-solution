package kr.co.jiin.solution.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("symbol")
public class SymbolMvcController {
    @RequestMapping("list")
    public String symbolListView(Model model){
        return "page/symbol/list";
    }

    @RequestMapping("preview")
    public String symbolPreview(Model model, @RequestParam String id){
        return "page/symbol/view";
    }

    @RequestMapping("create")
    public String symbolCreateView(Model model){
        return "page/symbol/form";
    }

    @RequestMapping("update")
    public String symbolUpdateView(Model model, @RequestParam String id){
        return "page/symbol/edit";
    }
}
