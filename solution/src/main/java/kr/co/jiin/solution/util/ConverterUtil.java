package kr.co.jiin.solution.util;

import org.json.JSONObject;
import org.json.XML;

public class ConverterUtil {
    public static String convertJSONToXML(String json){
        String xml = "";
        try {
            JSONObject jsonObj = new JSONObject(json);
            xml += XML.toString(jsonObj);
        } catch (Exception e) {
            e.printStackTrace();
            xml += "INVALID JSON STRING";
        }
        return xml;
    }
}
