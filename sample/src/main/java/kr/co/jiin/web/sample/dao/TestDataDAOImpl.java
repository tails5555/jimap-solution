package kr.co.jiin.web.sample.dao;

import kr.co.jiin.web.sample.dto.TestData;
import kr.co.jiin.web.sample.model.TestDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class TestDataDAOImpl implements TestDataDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    static class TestDataMapper implements RowMapper<TestData> {
        @Override
        public TestData mapRow(ResultSet rs, int rowNum) {
            try {
                return new TestData(rs.getLong("id"), rs.getInt("number"), rs.getDouble("decimal"), rs.getString("text"), rs.getDate("date"), rs.getBoolean("bool"));
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    @Override
    public List<TestData> findAll() {
        return jdbcTemplate.query("SELECT * FROM testdata", new BeanPropertyRowMapper<>(TestData.class));
    }

    @Override
    public long count() {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) from testdata", Long.class);
    }

    @Override
    public TestData findById(long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM testdata WHERE id = ?", new Object[] { id }, new TestDataMapper());
    }

    @Override
    public int create(TestDataModel testDataModel) {
        return jdbcTemplate.update("INSERT INTO testdata(number, text, date, bool, decimal) VALUES(?, ?, ?, ?, ?)", testDataModel.getNumber(), testDataModel.getText(), testDataModel.getDate(), testDataModel.getBool(), testDataModel.getDecimal());
    }

    @Override
    public int update(long id, TestDataModel testDataModel) {
        return jdbcTemplate.update("UPDATE testdata SET number = ?, text = ?, date = ?, bool = ?, decimal = ? WHERE id = ?", testDataModel.getNumber(), testDataModel.getText(), testDataModel.getDate(), testDataModel.getBool(), testDataModel.getDecimal(), id);
    }

    @Override
    public int deleteById(long id) {
        return jdbcTemplate.update("DELETE FROM testdata WHERE id = ?", id);
    }

    @Override
    public int deleteAll() {
        return jdbcTemplate.update("DELETE FROM testdata");
    }
}
