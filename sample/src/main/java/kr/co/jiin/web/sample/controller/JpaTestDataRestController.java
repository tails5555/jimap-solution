package kr.co.jiin.web.sample.controller;

import kr.co.jiin.web.sample.model.TestDataModel;
import kr.co.jiin.web.sample.service.TestDataJpaService;
import kr.co.jiin.web.sample.util.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

// PostgreSQL + Spring Data JPA 데이터 연동
@RestController
@RequestMapping("api/jpa/test")
public class JpaTestDataRestController {
    @Autowired
    private TestDataJpaService testDataService;

    @GetMapping("datas")
    public Map<String, Object> testDataList(){
        return RequestUtil.requestMap(testDataService.findAll());
    }

    @GetMapping("datas/counting")
    public Map<String, Object> testDataCounting(){
        return RequestUtil.requestMap(testDataService.count());
    }

    @GetMapping("data/{id}")
    public Map<String, Object> testDataElement(@PathVariable long id){
        return RequestUtil.requestMap(testDataService.findById(id));
    }

    @GetMapping("data/existing/{id}")
    public Map<String, Object> testDataExisting(@PathVariable long id){
        return RequestUtil.requestMap(testDataService.existsById(id));
    }

    @PostMapping("data")
    public Map<String, Object> testDataCreate(@RequestBody TestDataModel testDataModel){
        return RequestUtil.requestMap(testDataService.create(testDataModel));
    }

    @PutMapping("data/{id}")
    public Map<String, Object> testDataUpdate(@PathVariable long id, @RequestBody TestDataModel testDataModel){
        return RequestUtil.requestMap(testDataService.update(id, testDataModel));
    }

    @DeleteMapping("data/{id}")
    public Map<String, Object> testDataDelete(@PathVariable long id){
        return RequestUtil.requestMap(testDataService.deleteById(id));
    }

    @DeleteMapping("datas")
    public Map<String, Object> testDataDeleteAll(){
        return RequestUtil.requestMap(testDataService.deleteAll());
    }
}
