package kr.co.jiin.web.sample.dao;

import kr.co.jiin.web.sample.dto.TestData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestDataRepository extends CrudRepository<TestData, Long> {
}
