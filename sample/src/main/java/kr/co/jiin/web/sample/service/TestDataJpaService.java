package kr.co.jiin.web.sample.service;

import kr.co.jiin.web.sample.model.TestDataModel;
import kr.co.jiin.web.sample.vo.TestDataVO;

import java.util.List;

public interface TestDataJpaService {
    List<TestDataVO> findAll();
    TestDataVO findById(long id);
    TestDataVO create(TestDataModel testDataModel);
    TestDataVO update(long id, TestDataModel testDataModel);
    boolean deleteById(long id);
    boolean deleteAll();
    boolean existsById(long id);
    long count();
}
