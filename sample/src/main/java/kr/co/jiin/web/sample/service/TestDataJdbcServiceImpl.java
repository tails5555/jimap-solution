package kr.co.jiin.web.sample.service;

import kr.co.jiin.web.sample.dao.TestDataDAO;
import kr.co.jiin.web.sample.dto.TestData;
import kr.co.jiin.web.sample.model.TestDataModel;
import kr.co.jiin.web.sample.vo.TestDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TestDataJdbcServiceImpl implements TestDataJdbcService {
    @Autowired
    private TestDataDAO testDataDAO;

    @Override
    public List<TestDataVO> findAll() {
        List<TestData> list = testDataDAO.findAll();
        return list.stream()
                    .map(t -> TestDataVO.convertToVO(t))
                    .collect(Collectors.toList());
    }

    @Override
    public TestDataVO findById(long id) {
        TestData testData = testDataDAO.findById(id);
        if(testData != null){
            return TestDataVO.convertToVO(testData);
        } else {
            return null;
        }
    }

    @Override
    public boolean create(TestDataModel testDataModel) {
        return testDataDAO.create(testDataModel) > 0;
    }

    @Override
    public boolean update(long id, TestDataModel testDataModel) {
        return testDataDAO.update(id, testDataModel) > 0;
    }

    @Override
    public boolean deleteById(long id) {
        if(this.findById(id) != null) {
            testDataDAO.deleteById(id);
            return true;
        }
        else return false;
    }

    @Override
    public boolean deleteAll() {
        testDataDAO.deleteAll();
        return true;
    }

    @Override
    public long count(){
        return testDataDAO.count();
    }
}
