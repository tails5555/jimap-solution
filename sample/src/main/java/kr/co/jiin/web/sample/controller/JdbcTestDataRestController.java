package kr.co.jiin.web.sample.controller;

import kr.co.jiin.web.sample.model.TestDataModel;
import kr.co.jiin.web.sample.service.TestDataJdbcService;
import kr.co.jiin.web.sample.util.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

// PostgreSQL + Spring Data JDBC 데이터 연동
@RestController
@RequestMapping("api/jdbc/test")
public class JdbcTestDataRestController {
    @Autowired
    private TestDataJdbcService testDataJdbcService;

    @GetMapping("datas")
    public Map<String, Object> testDataList(){
        return RequestUtil.requestMap(testDataJdbcService.findAll());
    }

    @GetMapping("datas/counting")
    public Map<String, Object> testDataCounting(){
        return RequestUtil.requestMap(testDataJdbcService.count());
    }

    @GetMapping("data/{id}")
    public Map<String, Object> testDataElement(@PathVariable long id){
        return RequestUtil.requestMap(testDataJdbcService.findById(id));
    }

    @PostMapping("data")
    public Map<String, Object> testDataCreate(@RequestBody TestDataModel testDataModel){
        return RequestUtil.requestMap(testDataJdbcService.create(testDataModel));
    }

    @PutMapping("data/{id}")
    public Map<String, Object> testDataUpdate(@PathVariable long id, @RequestBody TestDataModel testDataModel){
        return RequestUtil.requestMap(testDataJdbcService.update(id, testDataModel));
    }

    @DeleteMapping("data/{id}")
    public Map<String, Object> testDataDelete(@PathVariable long id){
        return RequestUtil.requestMap(testDataJdbcService.deleteById(id));
    }

    @DeleteMapping("datas")
    public Map<String, Object> testDataDeleteAll(){
        return RequestUtil.requestMap(testDataJdbcService.deleteAll());
    }
}
