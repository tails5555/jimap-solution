package kr.co.jiin.web.sample.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Getter
@Setter
public class TestData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private Integer number;
    private Double decimal;
    private String text;
    private Date date;
    private Boolean bool;

    public TestData(){

    }

    public TestData(long id, Integer number, Double decimal, String text, Date date, Boolean bool){
        this.id = id;
        this.number = number;
        this.decimal = decimal;
        this.text = text;
        this.date = date;
        this.bool = bool;
    }
}
