package kr.co.jiin.web.sample.service;

import kr.co.jiin.web.sample.dao.TestDataRepository;
import kr.co.jiin.web.sample.dto.TestData;
import kr.co.jiin.web.sample.model.TestDataModel;
import kr.co.jiin.web.sample.vo.TestDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class TestDataJpaServiceImpl implements TestDataJpaService {
    @Autowired
    private TestDataRepository testDataRepository;

    @Override
    public List<TestDataVO> findAll() {
        Iterable<TestData> iterable = testDataRepository.findAll();
        return StreamSupport
                .stream(iterable.spliterator(), false)
                .map(t -> TestDataVO.convertToVO(t))
                .collect(Collectors.toList());
    }

    @Override
    public TestDataVO findById(long id) {
        Optional<TestData> optional = testDataRepository.findById(id);
        if(optional.isPresent()){
            return TestDataVO.convertToVO(optional.get());
        } else {
            return null;
        }
    }

    @Override
    public TestDataVO create(TestDataModel testDataModel) {
        TestData newData = TestDataModel.convertToDTO(0L, testDataModel);
        return TestDataVO.convertToVO(testDataRepository.save(newData));
    }

    @Override
    public TestDataVO update(long id, TestDataModel testDataModel) {
        TestData saveData = TestDataModel.convertToDTO(id, testDataModel);
        return TestDataVO.convertToVO(testDataRepository.save(saveData));
    }

    @Override
    public boolean deleteById(long id) {
        if(testDataRepository.existsById(id)) {
            testDataRepository.deleteById(id);
            return true;
        }
        else return false;
    }

    @Override
    public boolean deleteAll() {
        testDataRepository.deleteAll();
        return true;
    }

    @Override
    public boolean existsById(long id) {
        return testDataRepository.existsById(id);
    }

    @Override
    public long count() {
        return testDataRepository.count();
    }
}
