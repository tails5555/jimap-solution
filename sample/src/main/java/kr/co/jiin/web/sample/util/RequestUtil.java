package kr.co.jiin.web.sample.util;

import java.util.Collections;
import java.util.Map;

public class RequestUtil {
    private static final String DEFAULT_KEY = "result";

    public static Map<String, Object> requestMap(Object object){
        return Collections.singletonMap(DEFAULT_KEY, object);
    }
}
