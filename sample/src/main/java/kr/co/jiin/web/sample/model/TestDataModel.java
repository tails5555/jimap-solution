package kr.co.jiin.web.sample.model;

import kr.co.jiin.web.sample.dto.TestData;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class TestDataModel {
    private Integer number;
    private Double decimal;
    private String text;
    private Date date;
    private Boolean bool;

    public TestDataModel(){

    }

    public TestDataModel(Integer number, Double decimal, String text, Date date, Boolean bool){
        this.number = number;
        this.decimal = decimal;
        this.text = text;
        this.date = date;
        this.bool = bool;
    }

    public static TestData convertToDTO(long id, TestDataModel testDataModel){
        if(testDataModel == null){
            return null;
        } else {
            return new TestData(id, testDataModel.getNumber(), testDataModel.getDecimal(), testDataModel.getText(), testDataModel.getDate(), testDataModel.getBool());
        }
    }

    public static TestDataModel convertToModel(TestData testData){
        if(testData == null) {
            return null;
        } else {
            return new TestDataModel(testData.getNumber(), testData.getDecimal(), testData.getText(), testData.getDate(), testData.getBool());
        }
    }
}
