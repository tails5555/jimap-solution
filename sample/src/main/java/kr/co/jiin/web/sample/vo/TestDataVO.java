package kr.co.jiin.web.sample.vo;

import kr.co.jiin.web.sample.dto.TestData;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class TestDataVO {
    private long id;
    private int number;
    private double decimal;
    private String text;
    private Date date;
    private boolean bool;

    public TestDataVO(){

    }

    public TestDataVO(long id, int number, double decimal, String text, Date date, boolean bool){
        this.id = id;
        this.number = number;
        this.decimal = decimal;
        this.text = text;
        this.date = date;
        this.bool = bool;
    }

    public static TestDataVO convertToVO(TestData testData){
        if(testData == null) {
            return null;
        } else {
            return new TestDataVO(testData.getId(), testData.getNumber(), testData.getDecimal(), testData.getText(), testData.getDate(), testData.getBool());
        }
    }

    public static TestData convertToDTO(TestDataVO testDataVO){
        if(testDataVO == null){
            return null;
        } else {
            return new TestData(testDataVO.getId(), testDataVO.getNumber(), testDataVO.getDecimal(), testDataVO.getText(), testDataVO.getDate(), testDataVO.isBool());
        }
    }
}
