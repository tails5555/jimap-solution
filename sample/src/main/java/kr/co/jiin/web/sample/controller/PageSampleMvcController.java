package kr.co.jiin.web.sample.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("sample")
public class PageSampleMvcController {
    // Bootstrap 4 컴포넌트 적용 확인
    @RequestMapping("page/bootstrap")
    public String bootstrapPageSample(Model model) {
        return "page/sample/bootstrap";
    }

    // 비동기 컴포넌트 생성 : VueJS
    @RequestMapping("page/vuejs")
    public String vuejsPageSample(Model model) {
        return "page/sample/vuejs";
    }

    // 2차원 지도 도시 : Mapbox GL JS
    @RequestMapping("page/mapbox")
    public String mapboxPageSample(Model model) {
        return "page/sample/mapbox";
    }

    // 2차원 지도 도시 : Leaflet
    @RequestMapping("page/leaflet")
    public String leafletPageSample(Model model) {
        return "page/sample/leaflet";
    }

    // 2차원 지도 도시 : Openlayers
    @RequestMapping("page/openlayers")
    public String openlayersPageSample(Model model){
        return "page/sample/openlayers";
    }

    // 3차원 지도 도시 : CesiumJS
    @RequestMapping("page/cesium")
    public String cesiumjsPageSample(Model model){
        return "page/sample/cesium";
    }
}
