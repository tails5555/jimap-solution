package kr.co.jiin.web.sample.dao;

import kr.co.jiin.web.sample.dto.TestData;
import kr.co.jiin.web.sample.model.TestDataModel;

import java.util.List;

public interface TestDataDAO {
    List<TestData> findAll();
    long count();
    TestData findById(long id);
    int create(TestDataModel testDataModel);
    int update(long id, TestDataModel testDataModel);
    int deleteById(long id);
    int deleteAll();
}
