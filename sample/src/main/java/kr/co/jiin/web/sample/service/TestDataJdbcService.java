package kr.co.jiin.web.sample.service;

import kr.co.jiin.web.sample.model.TestDataModel;
import kr.co.jiin.web.sample.vo.TestDataVO;

import java.util.List;

public interface TestDataJdbcService {
    List<TestDataVO> findAll();
    TestDataVO findById(long id);
    boolean create(TestDataModel testDataModel);
    boolean update(long id, TestDataModel testDataModel);
    boolean deleteById(long id);
    boolean deleteAll();
    long count();
}
